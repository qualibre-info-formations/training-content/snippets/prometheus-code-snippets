const tasksumm = new prom.Summary ({
    name: 'forethought_requests_summ',
    help: 'Latency in percentiles',
});
const taskhisto = new prom.Histogram ({
    name: 'forethought_requests_hist',
    help: 'Latency in history form',
    // buckets: [0.1, 0.25, 0.5, 1, 2.5, 5, 10]
});
// ...
var responseTime = require('response-time');
// ...
app.use(responseTime(function (_, _, time) {
    tasksumm.observe(time);
    taskhisto.observe(time);
}));